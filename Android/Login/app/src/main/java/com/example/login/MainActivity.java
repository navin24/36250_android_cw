package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView Name;
    private TextView Password;
    private TextView Info;
    private Button Login;
    private int cnt = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Name = (TextView)findViewById(R.id.et1Name);
        Password = (TextView)findViewById(R.id.etPassword);
        Info = (TextView)findViewById(R.id.info);
        Login = (Button)findViewById(R.id.button);
        Info.setText("No. of remaining attempts: 5");

                  Login.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        validate(Name.getText().toString(), Password.getText().toString());
                    }
                });
            }


            private void validate(String userName, String userPassword)
            {
                if((userName.equals("Navin")) && (userPassword.equals("67891234")))
                {
                    Intent intent = new Intent(MainActivity.this, SecondaryActivity.class);
                    startActivity(intent);
                }else{
                    cnt--;

                    Info.setText("No. of remaining Attempts: " + String.valueOf(cnt));


                    if(cnt == 0){
                        Login.setEnabled(false);
                    }
                }
            }
        }