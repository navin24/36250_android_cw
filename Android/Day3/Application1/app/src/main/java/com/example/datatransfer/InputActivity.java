package com.example.datatransfer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InputActivity extends AppCompatActivity {

    EditText editFirstName, editLastName,editEmailAddress,editMobileNumber;
    Button saveButton , cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        editFirstName = findViewById(R.id.editFirstName);
        editLastName = findViewById(R.id.editLastName);
        editEmailAddress = findViewById(R.id.editEmailAddress);
        editMobileNumber = findViewById(R.id.editMobileNumber);
        saveButton = findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(InputActivity.this, ResultActivity.class );

                intent.putExtra("firstName",editFirstName.getText().toString());
                intent.putExtra("lastName" , editLastName.getText().toString());
                intent.putExtra("Email",editEmailAddress.getText().toString());
                intent.putExtra("Number", editMobileNumber.getText().toString());

                startActivity(intent);
            }
        });
        cancelButton = findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}