package com.example.datatransfer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    TextView textFirstName,textLastName,textEmail,textNumber;
    Button backButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        textFirstName = findViewById(R.id.textFirstName);
        textLastName= findViewById(R.id.textLastName);
        textEmail= findViewById(R.id.textEmail);
        textNumber=findViewById(R.id.textNumber);
        backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();

        String firstName = intent.getStringExtra("firstName");
        textFirstName.setText(firstName);
        String lastName = intent.getStringExtra("lastName");
        textLastName.setText(lastName);
        String email = intent.getStringExtra("Email");
        textEmail.setText(email);
        String number = intent.getStringExtra("Number");
        textNumber.setText(number);





    }
}