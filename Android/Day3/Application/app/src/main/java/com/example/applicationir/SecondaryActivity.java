package com.example.applicationir;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SecondaryActivity extends AppCompatActivity {

    TextView textFirstName , textLastName;
    Button buttonBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);

        textFirstName = findViewById(R.id.textFirstName);
        textLastName = findViewById(R.id.textLastName);
        buttonBack = findViewById(R.id.buttonBack);

        Intent intent = getIntent();

        String firstName = intent.getStringExtra("firstName");
        textFirstName.setText(firstName);
        String lastName = intent.getStringExtra("lastName");
        textLastName.setText(lastName);

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }
}