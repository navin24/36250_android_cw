package com.example.datarevese;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    TextView textFirstName , textLastName;
    Button dataButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        textFirstName = findViewById(R.id.textFirstName);
        textLastName = findViewById(R.id.textlastName);
        dataButton = findViewById(R.id.dataButton);

        dataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this , InputActivity.class);
                //startActivity(intent);
                startActivityForResult(intent , 0);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String firstName = data.getStringExtra("FirstName");
        String lastName = data.getStringExtra("LastName");

        textFirstName.setText(firstName);
        textLastName.setText(lastName);
    }
}