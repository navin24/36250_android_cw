import mysql.connector

def insert_data():
	connection = mysql.connector.connect(host="localhost",port=3306,user="root",password="root",database="desd_db")
	name='Navin'
	address='Mumbai'
	email='navin2@gmail.com'
	phone='+919013901310'

	query = f"insert into person (name,address,email,phone) values ('{name}','{address}','{email}','{phone}')"

	print(f"query = {query}")

	cursor = connection.cursor()

	cursor.execute(query)

	connection.commit()

	cursor.close()

	connection.close()

#insert_data()

def select_persons():
	connection = mysql.connector.connect(host="localhost",port=3306,user="root",password="root",database="desd_db")
	
	query = "select id, name,address,email,phone from person"

	cursor = connection.cursor()
	cursor.execute(query)

	persons=cursor.fetchall()

#	print(persons)
	for (id,name,address,email,phone) in persons:
		print(f"id: {id}")
		print(f"name: {name}")
		print(f"address: {address}")
		print(f"email: {email}")
		print(f"phone: {phone}")
		print(f"--------------")


	cursor.close()
	connection.close()

select_persons()
