from flask import Flask, jsonify, request
import mysql.connector

app = Flask(__name__)

@app.route("/", methods=["GET"])
def root():
  return "welcome to my REST APIs"


@app.route('/person', methods=["POST"])
def create_person():
  connection = mysql.connector.connect(host="localhost", user="root", password="root", database="desd_db", port=3306)
 
  name = request.form.get('name')
  email = request.form.get('email')
  address = request.form.get('address')
  phone = request.form.get('phone')
  query = f"insert into person (name, email, address, phone) values ('{name}', '{email}', '{address}', '{phone}')"
  
  cursor = connection.cursor()
  cursor.execute(query)
  connection.commit()

  cursor.close()
  connection.close()

  return "inserted successfully"


@app.route("/person", methods=["GET"])
def get_person():
  connection = mysql.connector.connect(host="localhost", user="root", password="root", database="desd_db", port=3306)
  
  query = "select id, name, email, address, phone from person"
  
  cursor = connection.cursor()
  cursor.execute(query)

  result = cursor.fetchall()

  persons = []
  for (id, name, email, address, phone) in result:
    persons.append({
      "id": id,
      "name": name,
      "address": address,
      "email": email,
      "phone": phone
    })

  cursor.close()
  connection.close()

  return jsonify(persons)


app.run(host="0.0.0.0", port=4000, debug=True)
