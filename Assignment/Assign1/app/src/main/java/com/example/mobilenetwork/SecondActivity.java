package com.example.mobilenetwork;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class SecondActivity extends AppCompatActivity {

    EditText editCompany , editModel , editRAM , editCamera , editPrice;
    Button saveButton , secondCancelButton;
    RadioButton editthreeg , editfourg, editfiveg;
    RadioGroup radioGroup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        editCompany = findViewById(R.id.editCompany);
        editModel = findViewById(R.id.editModel);
        editRAM = findViewById(R.id.editRAM);
        editCamera = findViewById(R.id.editCamera);
        editPrice = findViewById(R.id.editPrice);
        editthreeg = findViewById(R.id.editthreeg);
        editfourg = findViewById(R.id.editfourg);
        editfiveg = findViewById(R.id.editfiveg);
        saveButton = findViewById(R.id.saveButton);
        radioGroup = findViewById(R.id.radioGroup);
        secondCancelButton = findViewById(R.id.secondCancelButton);

        Intent intent = new Intent(SecondActivity.this , ThirdActivity.class);

        String mobileCompany = editCompany.getText().toString();
        String mobileModel = editModel.getText().toString();
        String mobileRAM = editRAM.getText().toString();
        String mobileCamera = editCamera.getText().toString();
        String mobilePrice = editPrice.getText().toString();


        intent.putExtra("Company",mobileCompany);
        intent.putExtra("Model",mobileModel);
        intent.putExtra("RAM",mobileRAM);
        intent.putExtra("Camera",mobileCamera);
        intent.putExtra("Price",mobilePrice);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent);
            }
        });

        secondCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}