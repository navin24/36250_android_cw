package com.example.mobilenetwork;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ThirdActivity extends AppCompatActivity {

      TextView textCompany , textModel,textRam,textCamera,textNetwork,textPrice;
      Button backButtton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        textCompany = findViewById(R.id.textCompany);
        textModel = findViewById(R.id.textModel);
        textRam = findViewById(R.id.textRam);
        textCamera = findViewById(R.id.textCamera);
        textPrice = findViewById(R.id.textPrice);
        textNetwork = findViewById(R.id.textNetwork);

        Intent intent = getIntent();

        String mobileCompany = intent.getStringExtra("Company");
        textCompany.setText(mobileCompany);
        String mobileModel = intent.getStringExtra("Model");
        textModel.setText(mobileModel);
        String mobileRAM = intent.getStringExtra("RAM");
        textRam.setText(mobileRAM);
        String mobileCamera = intent.getStringExtra("Camera");
        textCamera.setText(mobileCamera);
        String mobilePrice = intent.getStringExtra("Price");
        textPrice.setText(mobilePrice);
        String mobileNetwork = intent.getStringExtra("Network");
        textNetwork.setText(mobileNetwork);



        backButtton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}